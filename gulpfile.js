const {series} = require('gulp'),
      gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync').create();

function toComplileCss(){
    return gulp.src('./style/sass/*.scss')
           .pipe(sass())
           .pipe(gulp.dest('./style/css/'))
           .pipe(browserSync.stream());
}

function toRefresh(){
    browserSync.init({
        server:{
            baseDir: './'
        }
    });
    gulp.watch('./style/sass/*.scss',toComplileCss);
    gulp.watch('./animation.js').on('change',browserSync.reload)
    gulp.watch('./*.html').on('change',browserSync.reload);
}

exports.default = series(toComplileCss,toRefresh);