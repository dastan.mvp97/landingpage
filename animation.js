var waypoint = new Waypoint({
    element: document.getElementById('faceBeforeCollection'),
    handler: function(direction) {
        console.log('Scrolled to waypoint!')
        var tl = anime.timeline({
            easing: 'easeOutExpo',
            duration: 1500
        });
        tl.add({
            targets: '#div1',
            translateX: [
                {value:0,duration:500},
                {value:-1600,duration:500}
            ]
        }).add({
            targets: '#div2',
            translateX: [
                {value:0,duration:500},
                {value:-1500,duration:500}
            ],
        }).add({
            targets: '#div3',
            translateX: [
                {value:0,duration:500},
                {value:-1400,duration:500}
            ]
        });
        this.destroy();
    }
});

// Click event for Autumn image
document.getElementById('img1').addEventListener('click',function(){
    var matrix1 = $('#div1').css('transform').split(','),
        y1 = matrix1[13] || matrix1[5];
    if(y1 == " 0)"){
        createAddTextDiv1();
    }else {
        div1GoUp();
        document.getElementById('text1').classList.remove('fadeInUp');
        document.getElementById('text1').classList.add('animated','fadeOutDown');
    }
});

// Click event for Winter Image
document.getElementById('img2').addEventListener('click',function(){
    var pVar1 = document.getElementById('pVar1'),
        pVar2 = document.getElementById('pVar2');
    var matrix2 = $('#div2').css('transform').split(','),
        y2 = matrix2[13] || matrix2[5];
    if(y2 == " 0)") createAddTextDiv2();
    else{
        div2GoUp();
        pVar1.classList.remove('fadeInUp');
        pVar2.classList.remove('fadeInUp');
        pVar1.classList.add('fadeOutDown');
        pVar2.classList.add('fadeOutDown');
    }
});

// Click event for summer image
document.getElementById('img3').addEventListener('click',function(){
    var matrix3 = $('#div3').css('transform').split(','),
        y3 = matrix3[13] || matrix3[5];
    if(y3 == " 0)") createAddTextDiv3();
    else{
        div3GoUp();
        document.getElementById('text2').classList.remove('fadeInUp');
        document.getElementById('text2').classList.add('fadeOutDown');
    }
});

function createAddTextDiv1(){
    div1GoDown();

    var additionalText = document.getElementById('text1');
    additionalText.style.display = "inline-block";
    additionalText.classList.add('animated','fadeInUp');
    additionalText.classList.remove('fadeOutDown');
}


// Obtain additional text for Winter
function createAddTextDiv2(){
    div2GoDown();
    var pVar1 = document.getElementById('pVar1'),
        pVar2 = document.getElementById('pVar2');
    pVar1.style.display = "inline-block";
    pVar2.style.display = "inline-block";
    pVar1.classList.add('animated','fadeInUp');
    pVar2.classList.add('animated','fadeInUp');
    pVar1.classList.remove('fadeOutDown');
    pVar2.classList.remove('fadeOutDown');
}

// Obtain additional text for Summer
function createAddTextDiv3(){
    div3GoDown();

    var additionalText = document.getElementById('text2');
    additionalText.style.display = "inline-block";
    additionalText.classList.remove('fadeOutDown');
    additionalText.classList.add('animated','fadeInUp');
}

function div1GoUp(){
    anime({
        targets: '#div1,#forAnimeForm',
        translateY: {value:0,duration:2000}
    });
}

function div1GoDown(){
    anime({
        targets: '#div1,#forAnimeForm',
        translateY: {value:500,duration:2000}
    });
}

function div2GoUp(){
    anime({
        targets: '#div2,#forAnimeForm',
        translateY: {value:0,duration:2000}
    });
}

function div2GoDown(){
    anime({
        targets: '#div2,#forAnimeForm',
        translateY: {value:500,duration:2000}
    });
}

function div3GoUp(){
    anime({
        targets: '#div3,#forAnimeForm',
        translateY: {value:0,duration:2000}
    });
}

function div3GoDown(){
    anime({
        targets: '#div3,#forAnimeForm',
        translateY: {value:500,duration:2000}
    });
}

anime({
    targets: '#form',
    translateY:[{value:0, duration:1000},
                {value:-1500,duration:1000}]
});

   
